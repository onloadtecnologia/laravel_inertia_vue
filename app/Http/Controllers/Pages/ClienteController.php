<?php

namespace App\Http\Controllers\Pages;

use App\Http\Controllers\Controller;
use App\Models\Cliente;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Inertia\Inertia;

class ClienteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return  Inertia::render("Home", ["title" => "Home"]);
    }

    public function cadastro()
    {
        return  Inertia::render("Cadastro", ["title" => "Cadastro"]);
    }

    public function salvar(Request $request)
    {

            $cliente = new Cliente();
            $cliente->data = date("d/m/Y");
            $cliente->nome = $request->nome;
            $cliente->telefone = $request->telefone;
            $cliente->email = $request->email;
            $cliente->cep = $request->cep;
            if($cliente->save()){
                return  Redirect::route('clientes');
            }else{
                return  Redirect::route('cadastro'); 
            }

    }

    public function clientes()
    {
        $dados = Cliente::all();

        return  Inertia::render("Clientes", [
            "title" => "Clientes",
            "dados"=>$dados
        ]);
    }

    public function sobre()
    {
        return Inertia::render("Sobre",["title"=>"Sobre"]);
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        $cliente = Cliente::findOrFail($id);
        if($cliente->delete()){
            return Redirect::route("clientes");
        }else{
            return Redirect::route("cadastro");
        }
    }
}
