<?php

use App\Http\Controllers\Pages\ClienteController;
use Illuminate\Support\Facades\Route;




Route::get('/',[ClienteController::class,"index"])->name("index");
Route::get('/cadastro',[ClienteController::class,"cadastro"])->name("cadastro");
Route::post('/salvar',[ClienteController::class,"salvar"])->name("salvar");
Route::get('/clientes',[ClienteController::class,"clientes"])->name("clientes");
Route::delete('/deletar/{id}',[ClienteController::class,"destroy"])->name("deletar");
Route::get('/sobre',[ClienteController::class,"sobre"])->name("sobre");
